<!-- Instructions 
[ ] Update month, icon, week and dates
[ ] Add links to previous and next weekly issues
[ ] Update availability
[ ] Add goals
[ ] Daily Comments following format
```
# <DAY>

## :sunrise: At the beginning of the working day

### What would you like to accomplish?

* [ ]

## :city_sunset: At the end of the working day

### What have you accomplished?

*

### What have you learned?

* 

### What are the new action items?
* 

## :heart:  Self care

* [ ] Water :sweat_drops:
* [ ] Exercise :lifter: 
* [ ] Hot/Cold Therapy :fire: 🧊
* [ ] Supplement/Vitamins :pill: 
* [ ] Coffee :coffee:
* [ ] Healthy balanced eating :salad:
```
-->

# <MONTH> :icon: | MMM DD - MMM DD

[:arrow_backward:  Previous week](https://gitlab.com/jwormwell/jwormwell/-/issues/X) • [Next week :arrow_forward:](https://gitlab.com/jwormwell/jwormwell/-/issues/)

## :palm_tree: Availability / Out of office (OoO)

**Key:** 
:thermometer: Out Sick
:sun_with_face: Family and Friends Day
:palm_tree: Vacation
:ferris_wheel: Public Holiday 
:ok_hand_tone1: Normal schedule: 9:00-17:00
:level_slider: Different working hours

* :ok_hand_tone1: Monday 09:00 - 18:30
* :ok_hand_tone1: Tuesday 09:00 - 18:30
* :ok_hand_tone1: Wednesday 9:00 - 16:00
* :ok_hand_tone1: Thursday 9:00 - 17:00
* :ok_hand_tone1: Friday 9:00 - 16:00

_Read here about [my "normal" schedule and how I work](https://gitlab.com/jwormwell/jwormwell#how-i-work)_

## :dart:  Goals
 
### What 

* :one: 
* :two: 
* :three: 

<details>
<summary>
    My thinking behind this prioritisation
</summary>

**Facts:** 

1. 

**Analyzing the facts:**  

1. 

**My reasoning to pick these goals:**

1. 

</details>

### How  

1. 
    1. [ ] 


### What I would like to do, but may not be able to do...

1. [ ] 

## :crystal_ball: Sometime in the future

<details>
<summary>
    Expand
</summary>

---
**To-Do:**

:doughnut: 

* 

:construction_worker:

* 

**To-Learn:**

:headphones: 

* 

:computer: 

* 

</details>


## :star2: Wins of the week

* 

## :nerd: Learnings and wow moments

1. 
