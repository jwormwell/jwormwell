## James' README

Hi, I'm James, I'm currently a [Senior Demo Architect in EMEA](https://handbook.gitlab.com/job-families/sales/demo-architect/). This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

## What I'm currently working
- [Weekly priorities](https://gitlab.com/jwormwell/jwormwell/-/issues) - Goals of the week, high-level tasks, day-to-day progress, and learnings.

## Related pages

* [GitLab](https://gitlab.com/jwormwell)
* [LinkedIn](www.linkedin.com/in/james-wormwell)

## About me

I (re)joined GitLab in June 2024. Prior to joining GitLab I worked in a number of roles spanning the Software Development Lifecycle. I've worked in automotive, biotech, enterprise storage, high performance computing and IoT as a Software Engineer in Test, Software Engineer, "DevOps Engineer", DevOps Lead, Platform Architect and Head of Platform for Arm, Biorelate, Jaguar Land Rover and IBM. I have been a GitLab user since 2015 when IBM positioned it as the future of SCM within enterprise storage and an advocate ever since.

I live in [Ramsbottom, UK](https://goo.gl/maps/SBtLWfNFfrKCXXDF7) with my wife, four children, four ducks and dog.

## How I Work

I typically work 8am-6pm on Mondays and Tuesdays and 9am-4pm the rest of the week (Covering the school runs). The best way to reach me is on Slack, or tag me in a GitLab issue [`@jwormwell`](https://gitlab.com/jwormwell).

### Strengths/Weaknesses

The [High5Test](https://high5test.com/test/result-your-friend/MTcwNTQ2Ng==/) captures my strengths eerily well. I'm also a big fan of the 16 Personalities test. I'm an [INTJ-A (Assertive Architect)](https://www.16personalities.com/intj-personality) and can relate to the `Strengths & Weaknesses` section.

### Meetings

Whenver possible [I prefer to communicate async](/company/culture/all-remote/asynchronous/), but I'm happy to have a meeting if async is not effective or appropriate for a situation. If you'd like to invite me to a meeting, please give me a heads up and let me know what we'll be discussing. I work best when we have a meeting agenda shared ahead of time.
